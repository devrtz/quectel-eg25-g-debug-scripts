#!/bin/sh

# GPIO35 is PWRKEY
# GPIO68 is RESET_N
# GPIO231 is WAKEUP_IN on BH and AP_READY (active low) on CE
# GPIO232 is W_DISABLE#
# GPIO233 is STATUS

# DTR is:
# - PL6/GPIO358 on BH (1.1)
# - PB2/GPIO34 on CE (1.2)

# Power on sequence is as follows:
# Set RESET_N high
# Get STATUS high (floats to pull-up) #FIXME happens before PWRKEY sequence?
# Set WAKEUP_IN/AP_READY low (WAKEUP_IN for BH, AP_READY for CE)
# Set PWRKEY low for at least 500 ms then low
# UART becomes available after at least 12 s
# USB  becomes available after at least 13 s

if grep -q 1.1 /proc/device-tree/model
then
	DTR=358
else
	DTR=34
fi

echo "Enabling EG25 WWAN module"

# Configure outputs
for i in 35 68 231 232 $DTR
do
	echo "Setting GPIO$i\tas output"
	[ -e /sys/class/gpio/gpio$i ] && continue
	echo $i > /sys/class/gpio/export || return 1
	echo out > /sys/class/gpio/gpio$i/direction || return 1
done

# Configure inputs
for j in 233
do
	echo "Setting GPIO$j\tas input"
	[ -e /sys/class/gpio/gpio$j ] && continue
	echo $j > /sys/class/gpio/export || return 1
	echo in > /sys/class/gpio/gpio$j/direction || return 1
done

echo "Setting RESET_N high" # Inverted
echo 0 > /sys/class/gpio/gpio68/value || return 1
echo "Setting WAKEUP_IN/AP_READY low"
echo 0 > /sys/class/gpio/gpio231/value || return 1
echo "Setting W_DISABLE# high" # Inverted
echo 0 > /sys/class/gpio/gpio232/value || return 1
echo "Setting DTR low"
echo 0 > /sys/class/gpio/gpio$DTR/value || return 1

echo "Letting VBAT_BB rail stabilize"
sleep 0.06

echo "Performing PWRKEY sequence" # Inverted
( echo 1 > /sys/class/gpio/gpio35/value && sleep 2 && echo 0 > /sys/class/gpio/gpio35/value ) || return 1

echo -n "Waiting for modem to start..."
while [ $(cat /sys/class/gpio/gpio233/value) -eq "1" ]
do
	echo -n "."
	sleep 1
done
echo ""


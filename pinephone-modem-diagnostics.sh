#!/bin/sh

# GPIO35 is PWRKEY
# GPIO68 is RESET_N
# GPIO231 is WAKEUP_IN on BH and AP_READY (active low) on CE
# GPIO232 is W_DISABLE#
# GPIO233 is STATUS

# DTR is:
# - PL6/GPIO358 on BH (1.1)
# - PB2/GPIO34 on CE (1.2)

# Detect PinePhone version
if grep -q 1.1 /proc/device-tree/model ; then
	echo "Detected PinePhone v1.1"
	echo "WAKEUP_IN will be mislabled as AP_READY\n"
	DTR=358
elif grep -q 1.2 /proc/device-tree/model ; then
	echo "Detected PinePhone v1.2(a/b)\n"
	DTR=34
else
	echo "Failed to detect PinePhone version!"
	exit 1
fi



echo "### GPIO setup ###"
# Configure outputs
for i in 35 68 231 232 $DTR
do
	echo "Setting GPIO$i\tas output"
	[ -e /sys/class/gpio/gpio$i ] && continue
	echo $i > /sys/class/gpio/export || return 1
	echo out > /sys/class/gpio/gpio$i/direction || return 1
done

# Configure inputs
for j in 233
do
	echo "Setting GPIO$j\tas input"
	[ -e /sys/class/gpio/gpio$j ] && continue
	echo $j > /sys/class/gpio/export || return 1
	echo in > /sys/class/gpio/gpio$j/direction || return 1
done



# Detect expected power state
echo "\n#### POWER ###"
echo "Detecting expected power state:"

state_RESET_N=$(cat /sys/class/gpio/gpio68/value)
if [ "$state_RESET_N" -eq "0" ] ; then
	echo " RESET_N    high" # Inverted
else
	echo " RESET_N    low"
fi

state_AP_READY=$(cat /sys/class/gpio/gpio231/value)
if [ "$state_AP_READY" -eq "0" ] ; then
	echo " AP_READY   low"
else
	echo " AP_READY   high"
fi

state_W_DISABLE=$(cat /sys/class/gpio/gpio232/value)
if [ "$state_W_DISABLE" -eq "0" ] ; then
	echo " W_DISABLE  high"
else
	echo " W_DISABLE  low"
fi

state_DTR=$(cat /sys/class/gpio/gpio$DTR/value)
if [ "$state_DTR" -eq "0" ] ; then
	echo " DTR        low"
else
	echo " DTR        high"
fi

state_STATUS=$(cat /sys/class/gpio/gpio233/value)
if [ "$state_STATUS" -eq "0" ] ; then
	echo " STATUS     low"
else
	echo " STATUS     high"
fi

# Report state to the user
if [ "$state_RESET_N" -eq "0" ] && \
		[ "$state_AP_READY" -eq "0" ] && \
		[ "$state_W_DISABLE" -eq "0" ] && \
		[ "$state_DTR" -eq "0" ] && \
		[ "$state_STATUS" -eq 0 ] ; then
	echo "Modem is expected to be ON and reports ON"

elif [ "$state_RESET_N" -eq "0" ] && \
		[ "$state_AP_READY" -eq "0" ] && \
		[ "$state_W_DISABLE" -eq "0" ] && \
		[ "$state_DTR" -eq "0" ] && \
		[ "$state_STATUS" -eq 1 ] ; then
	echo "Modem is expected to be ON but reports OFF"

elif [ "$state_STATUS" -eq 0 ] ; then

	echo "Modem is expected to be OFF but reports ON"
else
	echo "Modem is expected to be OFF and reports OFF"
fi



echo "\n#### COMMUNICATION ###"
echo "Attempting to contact modem over UART:"
echo " AT:"
echo "AT" > /dev/ttyS2
read -t 1 RESPONSE < /dev/ttyS2
if [ "$RESPONSE" -eq "OK" ] ; then
	echo "Modem is responding on UART"
else
	echo "Modem is not responding on UART"
fi


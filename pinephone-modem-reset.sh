#!/bin/sh

# GPIO68 is RESET_N
# Sequence requires RESET_N to be pulled low for 150-460 ms

echo "Resetting EG25 WWAN module"
echo "Setting RESET_N low" # Inverted
echo 1 > /sys/class/gpio/gpio68/value
sleep 0.15 # Guarantees at least 150 ms
echo "Setting RESET_N high" # Inverted
echo 0 > /sys/class/gpio/gpio68/value


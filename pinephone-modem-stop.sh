#!/bin/sh

# GPIO35 is PWRKEY
# GPIO68 is RESET_N
# GPIO231 is WAKEUP_IN on BH and AP_READY (active low) on CE
# GPIO232 is W_DISABLE#
# GPIO233 is STATUS

# Power off secuence is as follows:
# Set PWRKEY low for at least 650 ms
# Get STATUS high when module has powered off

echo "Disabling EG25 WWAN module"

# Configure outputs
for i in 35 68 231 232 $DTR
do
        echo "Setting GPIO$i\tas output"
        [ -e /sys/class/gpio/gpio$i ] && continue
        echo $i > /sys/class/gpio/export || return 1
        echo out > /sys/class/gpio/gpio$i/direction || return 1
done

# Configure inputs
for j in 233
do
        echo "Setting GPIO$j\tas input"
        [ -e /sys/class/gpio/gpio$j ] && continue
        echo $j > /sys/class/gpio/export || return 1
        echo in > /sys/class/gpio/gpio$j/direction || return 1
done

echo "Setting RESET_N low" # Inverted
echo 1 > /sys/class/gpio/gpio68/value
echo "Setting W_DISABLE# low" # Inverted
echo 1 > /sys/class/gpio/gpio232/value

echo "Performing PWRKEY sequence" # Inverted
echo 1 > /sys/class/gpio/gpio35/value && sleep 2 && echo 0 > /sys/class/gpio/gpio35/value

echo -n "Waiting for modem to stop..."
while [ $(cat /sys/class/gpio/gpio233/value) -eq "0" ]
do
        echo -n "."
        sleep 1
done
echo ""

# Leave this in until the GPIO method is tested on a real modem
#sleep 30 # Wait for the module to power off
